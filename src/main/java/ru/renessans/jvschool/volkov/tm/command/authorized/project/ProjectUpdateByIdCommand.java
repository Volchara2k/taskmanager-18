package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectUpdateByIdCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    private static final String DESC_PROJECT_UPDATE_BY_ID = "обновить проект по идентификатору";

    private static final String NOTIFY_PROJECT_UPDATE_BY_ID =
            "Для обновления проекта по идентификатору введите идентификатор проекта из списка.\n " +
                    "Для обновления проекта введите его заголовок или заголовок с описанием.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_UPDATE_BY_ID);
        final String id = ViewUtil.getInstance().getLine();
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.updateById(userId, id, title, description);
        ViewUtil.getInstance().print(project);
    }

}