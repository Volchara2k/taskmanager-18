package ru.renessans.jvschool.volkov.tm.exception.illegal;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IllegalIndexException extends AbstractRuntimeException {

    private static final String INDEX_NOT_INTEGER =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    private static final String INDEX_ILLEGAL = "Ошибка! Параметр \"индекс\" является нелегальным!\n";

    public IllegalIndexException() {
        super(INDEX_ILLEGAL);
    }

    public IllegalIndexException(final String message) {
        super(String.format(INDEX_NOT_INTEGER, message));
    }

}