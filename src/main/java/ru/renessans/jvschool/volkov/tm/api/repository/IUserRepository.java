package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserRepository {

    User add(User user);

    User getById(String id);

    User getByLogin(String login);

    User deleteById(String id);

    User deleteByLogin(String login);

    User deleteByUser(User user);

    Collection<User> importData(Collection<User> users);

    Collection<User> exportData();

    void clearData();

}