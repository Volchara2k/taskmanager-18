package ru.renessans.jvschool.volkov.tm.exception.empty.model;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyDescriptionException extends AbstractRuntimeException {

    private static final String EMPTY_DESCRIPTION = "Ошибка! Параметр \"описание\" является пустым или null!\n";

    public EmptyDescriptionException() {
        super(EMPTY_DESCRIPTION);
    }

}