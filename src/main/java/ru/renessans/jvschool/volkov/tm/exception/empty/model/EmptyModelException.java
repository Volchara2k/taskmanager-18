package ru.renessans.jvschool.volkov.tm.exception.empty.model;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyModelException extends AbstractRuntimeException {

    private static final String EMPTY_ABSTRACT_MODEL = "Ошибка! Параметр \"модель\" является null!\n";

    public EmptyModelException() {
        super(EMPTY_ABSTRACT_MODEL);
    }

}