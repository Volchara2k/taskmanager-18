package ru.renessans.jvschool.volkov.tm.command.authorized.task;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class TaskListCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_TASK_LIST = "task-list";

    private static final String DESC_TASK_LIST = "вывод списка задач";

    private static final String NOTIFY_TASK_LIST = "Текущий список задач: \n";

    @Override
    public String getCommand() {
        return CMD_TASK_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_LIST;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_TASK_LIST);
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        final Collection<Task> tasks = taskService.getAll(userId);
        ViewUtil.getInstance().print(tasks);
    }

}