package ru.renessans.jvschool.volkov.tm.exception.security;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class AccessFailureException extends AbstractRuntimeException {

    private static final String ACCESS_FAILURE = "Ошибка! Сбой доступа!\n";

    private static final String ACCESS_FAILURE_FORMAT = "Ошибка! Возможные причины: %s\n";

    public AccessFailureException() {
        super(ACCESS_FAILURE);
    }

    public AccessFailureException(final String message) {
        super(String.format(ACCESS_FAILURE_FORMAT, message));
    }

}