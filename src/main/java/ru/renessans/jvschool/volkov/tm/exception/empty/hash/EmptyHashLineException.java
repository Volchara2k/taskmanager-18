package ru.renessans.jvschool.volkov.tm.exception.empty.hash;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyHashLineException extends AbstractRuntimeException {

    private static final String EMPTY_EMAIL = "Ошибка! Параметр \"строка для хеширования\" является пустым или null!\n";

    public EmptyHashLineException() {
        super(EMPTY_EMAIL);
    }

}