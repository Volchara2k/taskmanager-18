package ru.renessans.jvschool.volkov.tm.command.authstate;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractUnknownOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserSignUpCommand extends AbstractUnknownOnlyCommand {

    private static final String CMD_SIGN_UP = "sign-up";

    private static final String DESC_SIGN_UP = "зарегистрироваться в системе";

    private static final String NOTIFY_SIGN_UP = "Для регистрации пользователя в системе введите логин и пароль: \n";

    @Override
    public String getCommand() {
        return CMD_SIGN_UP;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_SIGN_UP;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_SIGN_UP);
        final String login = ViewUtil.getInstance().getLine();
        final String password = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final User user = authService.signUp(login, password);
        ViewUtil.getInstance().print(user);
    }

}