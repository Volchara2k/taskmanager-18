package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyPasswordException extends AbstractRuntimeException {

    private static final String EMPTY_PASSWORD = "Ошибка! Параметр \"пароль\" является пустым или null!\n";

    public EmptyPasswordException() {
        super(EMPTY_PASSWORD);
    }

}