package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.*;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.repository.*;
import ru.renessans.jvschool.volkov.tm.service.*;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);


    private final IAuthenticationRepository authRepository = new AuthenticationRepository();

    private final IAuthenticationService authService = new AuthenticationService(authRepository, userService);


    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);


    private final IOwnerRepository<Task> taskRepository = new TaskRepository();

    private final IOwnerService<Task> taskService = new TaskService(taskRepository);


    private final IOwnerRepository<Project> projectRepository = new ProjectRepository();

    private final IOwnerService<Project> projectService = new ProjectService(projectRepository);

    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService
    );

    {
        commandService.initCommands(this);
        final Collection<User> users = userService.initUserTestEntries();
        taskService.assignTestData(users);
        projectService.assignTestData(users);
    }

    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @Override
    public IOwnerService<Task> getTaskService() {
        return this.taskService;
    }

    @Override
    public IOwnerService<Project> getProjectService() {
        return this.projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    public void run(final String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(args);
    }

    private void terminalCommandPrintLoop() {
        String command = "";
        while (!DataConst.EXIT_FACTOR.equals(command)) {
            try {
                command = ScannerUtil.getLine();
                final AbstractCommand abstractCommand = this.commandService.getTerminalCommand(command);

                if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(command);
                authService.verifyPermissions(abstractCommand.permissions());

                abstractCommand.execute();
            } catch (final Exception e) {
                System.err.print(e.getMessage());
            }
        }
    }

    private void argumentPrint(final String... args) {
        try {
            final String arg = args[0];
            final AbstractCommand abstractCommand = this.commandService.getArgumentCommand(arg);

            if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(arg);
            authService.verifyPermissions(abstractCommand.permissions());

            abstractCommand.execute();
        } catch (final Exception e) {
            System.err.print(e.getMessage());
        }
    }

}