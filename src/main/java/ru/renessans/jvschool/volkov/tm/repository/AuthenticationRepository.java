package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.model.User;

public final class AuthenticationRepository implements IAuthenticationRepository {

    private String userId;

    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public void subscribe(final User user) {
        this.userId = user.getId();
    }

    @Override
    public boolean unsubscribe() {
        this.userId = null;
        return true;
    }

}