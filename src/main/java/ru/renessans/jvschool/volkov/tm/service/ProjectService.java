package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.model.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.model.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.model.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.model.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class ProjectService implements IOwnerService<Project> {

    private final IOwnerRepository<Project> projectRepository;

    public ProjectService(final IOwnerRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = new Project(title, description);
        return this.projectRepository.add(userId, project);
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project deleteByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project deleteById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.removeById(userId, id);
    }

    @Override
    public Project deleteByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.removeByTitle(userId, title);
    }

    @Override
    public Collection<Project> deleteAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.removeAll(userId);
    }

    @Override
    public Project getByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.getByIndex(userId, index);
    }

    @Override
    public Project getById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.getById(userId, id);
    }

    @Override
    public Project getByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(userId, title);
    }

    @Override
    public Collection<Project> getAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.getAll(userId);
    }

    @Override
    public Collection<Project> assignTestData(final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        final List<Project> assignedData = new ArrayList<>();
        users.forEach(user -> {
            final Project project =
                    add(user.getId(), DataConst.PROJECT_TITLE, DataConst.PROJECT_DESCRIPTION);
            assignedData.add(project);
        });

        return assignedData;
    }

    @Override
    public Collection<Project> importData(final Collection<Project> projects) {
        if (ValidRuleUtil.isNullOrEmpty(projects)) return null;
        return this.projectRepository.importData(projects);
    }

    @Override
    public Collection<Project> exportData() {
        return this.projectRepository.exportData();
    }

}