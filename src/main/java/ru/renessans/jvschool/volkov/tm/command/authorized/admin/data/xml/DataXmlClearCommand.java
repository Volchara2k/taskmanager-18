package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.xml;

import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlClearCommand extends AbstractDataCommand {

    private static final String CMD_XML_CLEAR = "data-xml-clear";

    private static final String DESC_XML_CLEAR = "очистить xml данные";

    private static final String NOTIFY_XML_CLEAR = "Происходит процесс очищения xml данных...";

    @Override
    public String getCommand() {
        return CMD_XML_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_XML_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_XML_CLEAR);
        final Path path = Paths.get(XML_FILE_LOCATE);
        final boolean removeState = FileUtil.deleteFile(path);
        ViewUtil.getInstance().print(removeState);
    }

}