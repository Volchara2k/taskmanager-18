package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectCreateCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_CREATE = "project-create";

    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    private static final String NOTIFY_PROJECT_CREATE =
            "Для создания проекта введите его заголовок или заголовок с описанием.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_CREATE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_CREATE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_CREATE);
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.add(userId, title, description);
        ViewUtil.getInstance().print(project);
    }

}