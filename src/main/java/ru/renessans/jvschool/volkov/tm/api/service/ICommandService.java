package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> initCommands(IServiceLocator serviceLocator);

    Collection<AbstractCommand> getAllCommands();

    Collection<AbstractCommand> getAllTerminalCommands();

    Collection<AbstractCommand> getAllArgumentCommands();

    AbstractCommand getTerminalCommand(final String command);

    AbstractCommand getArgumentCommand(final String command);

}