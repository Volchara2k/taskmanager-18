package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthenticationRepository {

    String getUserId();

    void subscribe(User user);

    boolean unsubscribe();

}