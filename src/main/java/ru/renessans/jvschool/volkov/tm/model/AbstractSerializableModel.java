package ru.renessans.jvschool.volkov.tm.model;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractSerializableModel implements Serializable {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.id;
    }

}