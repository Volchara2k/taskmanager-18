package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyLastNameException extends AbstractRuntimeException {

    private static final String EMPTY_LAST_NAME = "Ошибка! Параметр \"фамилия\" является пустым или null!\n";

    public EmptyLastNameException() {
        super(EMPTY_LAST_NAME);
    }

}