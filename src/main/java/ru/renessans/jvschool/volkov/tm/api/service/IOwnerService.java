package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IOwnerService<T> {

    T add(String userId, String title, String description);

    T updateByIndex(String userId, Integer index, String title, String description);

    T updateById(String userId, String id, String title, String description);

    T deleteByIndex(String userId, Integer index);

    T deleteById(String userId, String id);

    T deleteByTitle(String userId, String title);

    Collection<T> deleteAll(String userId);

    T getByIndex(String userId, Integer index);

    T getById(String userId, String id);

    T getByTitle(String userId, String title);

    Collection<T> getAll(String userId);

    Collection<T> assignTestData(Collection<User> users);

    Collection<T> importData(Collection<T> objects);

    Collection<T> exportData();

}