package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.empty.command.EmptyCommandException;

import java.util.*;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> initCommands(final IServiceLocator serviceLocator) {
        final Collection<AbstractCommand> commands = this.commandRepository.getAllCommands();
        commands.forEach(command -> command.setServiceLocator(serviceLocator));
        return commands;
    }

    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return this.commandRepository.getAllCommands();
    }

    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return this.commandRepository.getAllTerminalCommands();
    }

    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return this.commandRepository.getAllArgumentCommands();
    }

    @Override
    public AbstractCommand getTerminalCommand(final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getTerminalCommand(command);
    }

    @Override
    public AbstractCommand getArgumentCommand(final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getArgumentCommand(command);
    }

}