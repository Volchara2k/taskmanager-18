package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getAllCommands();

    Collection<AbstractCommand> getAllTerminalCommands();

    Collection<AbstractCommand> getAllArgumentCommands();

    AbstractCommand getArgumentCommand(String command);

    AbstractCommand getTerminalCommand(String command);

}