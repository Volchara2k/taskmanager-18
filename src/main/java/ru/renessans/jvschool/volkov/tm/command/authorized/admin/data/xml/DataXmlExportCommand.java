package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.xml;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchange;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.List;

public final class DataXmlExportCommand extends AbstractDataCommand {

    private static final String CMD_XML_EXPORT = "data-xml-export";

    private static final String DESC_XML_EXPORT = "экспортировать домен в xml вид";

    private static final String NOTIFY_XML_EXPORT = "Происходит процесс выгрузки домена в xml вид...";

    @Override
    public String getCommand() {
        return CMD_XML_EXPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_XML_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_XML_EXPORT);

        final Domain domain = new Domain();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        try (final DataInterchange<Domain> dataInterchange = new DataInterchange<>()) {
            dataInterchange.writeToXml(domain, XML_FILE_LOCATE);
        }

        final List<User> users = (List<User>) domain.getUserList();
        ViewUtil.getInstance().print(users);
        final List<Task> tasks = (List<Task>) domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final List<Project> projects = (List<Project>) domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}