package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary;

import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBinaryClearCommand extends AbstractDataCommand {

    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    @Override
    public String getCommand() {
        return CMD_BIN_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BIN_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_BIN_CLEAR);
        final Path path = Paths.get(BIN_FILE_LOCATE);
        final boolean removeState = FileUtil.deleteFile(path);
        ViewUtil.getInstance().print(removeState);
    }

}