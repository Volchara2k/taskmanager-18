package ru.renessans.jvschool.volkov.tm.command.authorized.task;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskCreateCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_TASK_CREATE = "task-create";

    private static final String DESC_TASK_CREATE = "добавить новую задачу";

    private static final String NOTIFY_TASK_CREATE =
            "Для создания задачи введите её заголовок или заголовок с описанием.";

    @Override
    public String getCommand() {
        return CMD_TASK_CREATE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_CREATE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_TASK_CREATE);
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        final Task task = taskService.add(userId, title, description);
        ViewUtil.getInstance().print(task);
    }

}