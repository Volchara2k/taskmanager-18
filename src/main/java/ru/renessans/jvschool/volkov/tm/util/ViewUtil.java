package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.exception.empty.model.EmptyModelException;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.exception.security.UserLogOutFailureException;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public final class ViewUtil {

    private static final String SUCCESSFUL_OUTCOME = "Операция УСПЕШНО завершилась!\n";

    private static final String UNSUCCESSFUL_OUTCOME = "Операция завершилась с ОШИБКОЙ!\n";

    private static volatile ViewUtil INSTANCE;

    public static ViewUtil getInstance() {
        final ViewUtil result = INSTANCE;
        if (!Objects.isNull(result)) return result;

        synchronized (ViewUtil.class) {
            if (Objects.isNull(INSTANCE)) INSTANCE = new ViewUtil();
            return INSTANCE;
        }
    }

    private ViewUtil() {
    }

    public void print(final String string) {
        System.out.println(string);
    }

    public void print(final boolean state) {
        if (!state) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new UserLogOutFailureException();
        }

        print(SUCCESSFUL_OUTCOME);
    }

    public void print(final Collection<?> collection) {
        if (ValidRuleUtil.isNullOrEmpty(collection)) {
            print("Список на текущий момент пуст.");
            return;
        }

        final AtomicInteger index = new AtomicInteger(1);
        collection.forEach(element -> {
            print(index + ". " + element);
            index.getAndIncrement();
        });

        print(SUCCESSFUL_OUTCOME);
    }

    public void print(final AbstractSerializableModel model) {
        if (Objects.isNull(model)) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new EmptyModelException();
        }

        print(model.toString());
    }

    public void print(final AuthState authState) {
        if (Objects.isNull(authState)) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException();
        }

        if (!authState.isSuccess()) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException(authState.getTitle());
        }

        print(SUCCESSFUL_OUTCOME);
    }

    public String getLine() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getLine();
    }

    public Integer getInteger() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getInteger();
    }

}