package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyEmailException extends AbstractRuntimeException {

    private static final String EMPTY_EMAIL = "Ошибка! Параметр \"электронная почта\" является пустым или null!\n";

    public EmptyEmailException() {
        super(EMPTY_EMAIL);
    }

}