package ru.renessans.jvschool.volkov.tm.command.authorized.admin.usermanage;

import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAdminOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserDeleteCommand extends AbstractAdminOnlyCommand {

    private static final String CMD_USER_DELETE = "user-delete";

    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    private static final String NOTIFY_USER_DELETE = "Для удаления пользователя в системе введите его логин. \n";

    @Override
    public String getCommand() {
        return CMD_USER_DELETE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_USER_DELETE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_USER_DELETE);
        final String login = ViewUtil.getInstance().getLine();
        final IUserService userService = super.serviceLocator.getUserService();
        final User user = userService.deleteUserByLogin(login);
        ViewUtil.getInstance().print(user);
    }

}