package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.json;

import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataJsonClearCommand extends AbstractDataCommand {

    private static final String CMD_JSON_CLEAR = "data-json-clear";

    private static final String DESC_JSON_CLEAR = "очистить json данные";

    private static final String NOTIFY_JSON_CLEAR = "Происходит процесс очищения json данных...";

    @Override
    public String getCommand() {
        return CMD_JSON_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_JSON_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_JSON_CLEAR);
        final Path path = Paths.get(JSON_FILE_LOCATE);
        final boolean removeState = FileUtil.deleteFile(path);
        ViewUtil.getInstance().print(removeState);
    }

}