package ru.renessans.jvschool.volkov.tm.command.abstraction;

import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;

public abstract class AbstractAuthOnlyCommand extends AbstractAdminOnlyCommand {

    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN, UserRole.USER};
    }

}