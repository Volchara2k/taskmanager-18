package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.json;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchange;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;
import java.util.Objects;

public final class DataJsonImportCommand extends AbstractDataCommand {

    private static final String CMD_JSON_IMPORT = "data-json-import";

    private static final String DESC_JSON_IMPORT = "импортировать домен из json вида";

    private static final String NOTIFY_JSON_IMPORT = "Происходит процесс загрузки домена из json вида...";

    @Override
    public String getCommand() {
        return CMD_JSON_IMPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_JSON_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_JSON_IMPORT);

        final Domain domain;
        try (final DataInterchange<Domain> dataInterchange = new DataInterchange<>()) {
            domain = dataInterchange.readFromJson(JSON_FILE_LOCATE, Domain.class);
        }

        if (Objects.isNull(domain)) return;
        final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        final Collection<User> users = domain.getUserList();
        ViewUtil.getInstance().print(users);
        final Collection<Task> tasks = domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final Collection<Project> projects = domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}