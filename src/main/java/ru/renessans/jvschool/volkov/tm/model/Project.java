package ru.renessans.jvschool.volkov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends AbstractSerializableModel {

    private static final long serialVersionUID = 1L;

    private String title = "";

    private String description = "";

    private String userId;

    public Project() {
    }

    public Project(final String title) {
        this.title = title;
    }

    public Project(final String title, final String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (!Objects.isNull(this.title))
            result.append("Заголовок проекта: ").append(this.title);
        if (!Objects.isNull(this.description))
            result.append(", описание проекта: ").append(this.description);
        if (!Objects.isNull(super.getId())) {
            result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        }
        return result.toString();
    }

}