package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.json;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchange;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.List;

public final class DataJsonExportCommand extends AbstractDataCommand {

    private static final String CMD_JSON_EXPORT = "data-json-export";

    private static final String DESC_JSON_EXPORT = "экспортировать домен в json вид";

    private static final String NOTIFY_JSON_EXPORT = "Происходит процесс выгрузки домена в json вид...";

    @Override
    public String getCommand() {
        return CMD_JSON_EXPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_JSON_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_JSON_EXPORT);

        final Domain domain = new Domain();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        try (final DataInterchange<Domain> dataInterchange = new DataInterchange<>()) {
            dataInterchange.writeToJson(domain, JSON_FILE_LOCATE);
        }

        final List<User> users = (List<User>) domain.getUserList();
        ViewUtil.getInstance().print(users);
        final List<Task> tasks = (List<Task>) domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final List<Project> projects = (List<Project>) domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}