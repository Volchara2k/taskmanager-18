package ru.renessans.jvschool.volkov.tm.command.authorized.profile;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileEditCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_EDIT_PROFILE = "edit-profile";

    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    private static final String NOTIFY_EDIT_PROFILE =
            "Для обновления данных пользователя введите его имя или имя с фамилией: \n";

    @Override
    public String getCommand() {
        return CMD_EDIT_PROFILE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_EDIT_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_EDIT_PROFILE);
        final String firstName = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IUserService userService = super.serviceLocator.getUserService();
        final User user = userService.editProfileById(userId, firstName);
        ViewUtil.getInstance().print(user);
    }

}