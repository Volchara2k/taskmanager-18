package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyDataObjectException extends AbstractRuntimeException {

    private static final String EMPTY_FILE =
            "Ошибка! Параметр \"объект обмена данных\" является null!\n";

    public EmptyDataObjectException() {
        super(EMPTY_FILE);
    }

}