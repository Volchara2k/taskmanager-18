package ru.renessans.jvschool.volkov.tm.command.authorized.task;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByTitleCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_TASK_DELETE_BY_TITLE = "task-delete-by-title";

    private static final String DESC_TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    private static final String NOTIFY_TASK_DELETE_BY_TITLE =
            "Для удаления задачи по заголовку введите имя заголовок из списка ниже.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_TASK_DELETE_BY_TITLE);
        final String title = ViewUtil.getInstance().getLine();
        final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        final String userId = authService.getUserId();
        final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        final Task task = taskService.deleteByTitle(userId, title);
        ViewUtil.getInstance().print(task);
    }

}