package ru.renessans.jvschool.volkov.tm.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public final class FileUtil {

    public static File createFile(final String filename) throws IOException {
        final File file = new File(filename);
        deleteFile(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    public static byte[] readFile(final String filename) throws IOException {
        return Files.readAllBytes(Paths.get(filename));
    }

    public static boolean deleteFile(final Path path) throws IOException {
        return Files.deleteIfExists(path);
    }

    public static boolean fileIsNotExists(final File file) {
        return !file.exists() || file.isDirectory();
    }

    public static boolean fileIsNotExists(final String fileName) {
        final File file = new File(fileName);
        return fileIsNotExists(file);
    }

    private FileUtil() {
    }

}