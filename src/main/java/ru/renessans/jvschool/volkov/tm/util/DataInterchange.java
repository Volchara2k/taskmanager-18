package ru.renessans.jvschool.volkov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDataObjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyFileException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyFileNameException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class DataInterchange<T> implements Closeable {

    private ObjectInputStream objectInputStream;

    private ObjectOutputStream objectOutputStream;

    private ByteArrayInputStream byteArrayInputStream;

    private ByteArrayOutputStream byteArrayOutputStream;

    private FileInputStream fileInputStream;

    private FileOutputStream fileOutputStream;

    @Override
    public void close() throws IOException {
        if (!Objects.isNull(this.objectOutputStream)) this.objectOutputStream.close();
        if (!Objects.isNull(this.objectInputStream)) this.objectInputStream.close();
        if (!Objects.isNull(this.byteArrayOutputStream)) this.byteArrayOutputStream.close();
        if (!Objects.isNull(this.byteArrayInputStream)) this.byteArrayInputStream.close();
        if (!Objects.isNull(this.fileOutputStream)) this.fileOutputStream.close();
        if (!Objects.isNull(this.fileInputStream)) this.fileInputStream.close();
    }

    public void writeToBin(final T object, final String filename) throws IOException {
        if (Objects.isNull(object)) throw new EmptyDataObjectException();
        if (Objects.isNull(filename)) throw new EmptyFileException();

        final File file = FileUtil.createFile(filename);
        this.fileOutputStream = new FileOutputStream(file);

        writeAsFile(object, fileOutputStream);
    }

    public T readFromBin(final String filename, final Class<T> tClass) throws IOException, ClassNotFoundException {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFileNameException();
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        if (Objects.isNull(tClass)) throw new EmptyDataObjectException();

        this.fileInputStream = new FileInputStream(filename);
        return readObject(fileInputStream, tClass);
    }

    public void writeToBase64(final T t, final String filename) throws IOException {
        if (Objects.isNull(t)) throw new EmptyDataObjectException();
        if (Objects.isNull(filename)) throw new EmptyFileException();

        this.byteArrayOutputStream = new ByteArrayOutputStream();
        writeAsFile(t, byteArrayOutputStream);

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        writeAsFile(base64Bytes, filename);
    }

    public T readFromBase64(final String filename, final Class<T> tClass) throws IOException, ClassNotFoundException {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFileNameException();
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        if (Objects.isNull(tClass)) throw new EmptyDataObjectException();

        final byte[] fileBytes = FileUtil.readFile(filename);
        final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        this.byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        return readObject(this.byteArrayInputStream, tClass);
    }

    public void writeToJson(final T t, final String filename) throws IOException {
        if (Objects.isNull(t)) throw new EmptyDataObjectException();
        if (Objects.isNull(filename)) throw new EmptyFileException();

        final ObjectMapper objectMapper = new ObjectMapper();
        final String jsonFormat = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(t);
        final byte[] jsonBytes = jsonFormat.getBytes(StandardCharsets.UTF_8);

        writeAsFile(jsonBytes, filename);
    }

    public T readFromJson(final String filename, final Class<T> tClass) throws IOException {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFileNameException();
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        if (Objects.isNull(tClass)) throw new EmptyDataObjectException();

        this.fileInputStream = new FileInputStream(filename);
        final ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(fileInputStream, tClass);
    }

    public void writeToXml(final T t, final String filename) throws IOException {
        if (Objects.isNull(t)) throw new EmptyDataObjectException();
        if (Objects.isNull(filename)) throw new EmptyFileException();

        final ObjectMapper objectMapper = new XmlMapper();
        final String xmlFormat = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(t);
        final byte[] xmlBytes = xmlFormat.getBytes(StandardCharsets.UTF_8);

        writeAsFile(xmlBytes, filename);
    }

    public T readFromXml(final String filename, final Class<T> tClass) throws IOException {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFileNameException();
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        if (Objects.isNull(tClass)) throw new EmptyDataObjectException();

        this.fileInputStream = new FileInputStream(filename);
        final ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(fileInputStream, tClass);
    }

    private T readObject(final InputStream inputStream, final Class<T> tClass) throws IOException, ClassNotFoundException {
        this.objectInputStream = new ObjectInputStream(inputStream);
        return tClass.cast(this.objectInputStream.readObject());
    }

    private void writeAsFile(final T t, final OutputStream outputStream) throws IOException {
        this.objectOutputStream = new ObjectOutputStream(outputStream);
        this.objectOutputStream.writeObject(t);
        this.objectOutputStream.flush();
    }

    private void writeAsFile(final byte[] bytes, final String filename) throws IOException {
        final File file = FileUtil.createFile(filename);
        this.fileOutputStream = new FileOutputStream(file.getName());
        this.fileOutputStream.write(bytes);
        this.fileOutputStream.flush();
    }

}