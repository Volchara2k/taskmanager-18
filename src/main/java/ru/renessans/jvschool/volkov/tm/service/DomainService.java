package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.Objects;

public final class DomainService implements IDomainService {

    private final IUserService userService;

    private final IOwnerService<Task> taskService;

    private final IOwnerService<Project> projectService;

    public DomainService(
            final IUserService userService,
            final IOwnerService<Task> taskService,
            final IOwnerService<Project> projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void dataImport(final Domain domain) {
        if (Objects.isNull(domain)) return;
        this.userService.importData(domain.getUserList());
        this.taskService.importData(domain.getTaskList());
        this.projectService.importData(domain.getProjectList());
    }

    @Override
    public void dataExport(final Domain domain) {
        if (Objects.isNull(domain)) return;
        domain.setUserList(this.userService.getExportData());
        domain.setTaskList(this.taskService.exportData());
        domain.setProjectList(this.projectService.exportData());
    }

}