package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyFileNameException extends AbstractRuntimeException {

    private static final String EMPTY_FILE_NAME = "Ошибка! Параметр \"имя файла\" является пустым или null!\n";

    public EmptyFileNameException() {
        super(EMPTY_FILE_NAME);
    }

}