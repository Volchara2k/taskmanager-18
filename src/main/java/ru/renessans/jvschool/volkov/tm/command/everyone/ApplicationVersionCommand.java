package ru.renessans.jvschool.volkov.tm.command.everyone;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ApplicationVersionCommand extends AbstractCommand {

    private static final String CMD_VERSION = "version";

    private static final String ARG_VERSION = "-v";

    private static final String DESC_VERSION = "вывод версии программы";

    private static final String NOTIFY_VERSION = "Версия: 1.0.18.";

    @Override
    public String getCommand() {
        return CMD_VERSION;
    }

    @Override
    public String getArgument() {
        return ARG_VERSION;
    }

    @Override
    public String getDescription() {
        return DESC_VERSION;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_VERSION);
    }

}