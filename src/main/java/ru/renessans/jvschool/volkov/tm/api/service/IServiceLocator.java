package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;

public interface IServiceLocator {

    IUserService getUserService();

    IAuthenticationService getAuthenticationService();

    IOwnerService<Task> getTaskService();

    IOwnerService<Project> getProjectService();

    ICommandService getCommandService();

    IDomainService getDomainService();

}