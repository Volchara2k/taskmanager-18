package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyUserRoleException extends AbstractRuntimeException {

    private static final String EMPTY_USER_ROLE = "Ошибка! Параметр \"тип пользователя\" является null!\n";

    public EmptyUserRoleException() {
        super(EMPTY_USER_ROLE);
    }

}